#include <Arduino.h>
#define TESTPIN A0
#define cdi(sfr,bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr,bit) (_SFR_BYTE(sfr) |= _BV(bit))

volatile int counter;

int main(void) {
Serial.begin(9600);
sbi(PCICR, PCIE1);
sbi(PCMSK1, PCINT8);
pinMode(TESTPIN, INPUT_PULLUP);
  while(1){
    Serial.println(counter);
    analogWrite(5, 100);
  }
  return 0;
}

ISR(PCINT2_vect){
  counter ++;
}
